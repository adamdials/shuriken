#pragma strict

var p3_1 : GameObject;
var p3_2 : GameObject;
var counter : int;
var rigidParticlesCount : int;
var autoAddRigidParticles : boolean;
var autoAddScene : boolean;
var FPS : float;

function Start()
{
	counter = 0;
	rigidParticlesCount = 2;
	p3_1 = GameObject.Find("p3_1") as GameObject;
	p3_2 = GameObject.Find("p3_2") as GameObject;
	autoAddRigidParticles = true;
}

function Update()
{
	if(autoAddRigidParticles)
	{
		if(counter++==300)
		{
			counter=0;
			var rand : int = Random.Range(0,2);			
			AddRigidParticle(rand);
			AddSceneAditiveWithParticles();
		}
	}
	else { counter = 0; }
	
	FPS = 1/Time.deltaTime;
}

function AddRigidParticle(n : int)
{
	rigidParticlesCount++;
	var go : GameObject;
	
	if(n==1)
	{
		go = Instantiate(p3_1,p3_1.transform.position + Vector3(0,1,0),p3_1.transform.rotation);
	}
	else
	{	
		go = Instantiate(p3_2,p3_2.transform.position + Vector3(0,1,0),p3_2.transform.rotation);
	}
}

function AddSceneAditiveWithParticles()
{
	rigidParticlesCount++;
	Application.LoadLevelAdditive("MoreParticles");
}

function OnGUI()
{
	if(GUI.Button(new Rect(10,10,100,30),"Restart"))
	{
		rigidParticlesCount = 0;
		Application.LoadLevel("MainScene");
	}
	
	if(GUI.Button(new Rect(10,50,200,30),"Auto Add Particles: "+autoAddRigidParticles))
	{
		if(autoAddRigidParticles)
			autoAddRigidParticles = false;
		else
			autoAddRigidParticles = true;
	}
	
	if(GUI.Button(new Rect(10,90,200,30),"Add More Particles"))
	{
		AddSceneAditiveWithParticles();
		AddRigidParticle(Random.Range(0,2));
	}
	
	GUI.Label(new Rect(120,7,200,30),"Stress Count: "+rigidParticlesCount);
	GUI.Label(new Rect(120,23,200,30),"FPS: "+FPS.ToString("0"));
}